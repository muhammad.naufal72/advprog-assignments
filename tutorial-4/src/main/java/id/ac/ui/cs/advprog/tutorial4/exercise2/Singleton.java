package id.ac.ui.cs.advprog.tutorial4.exercise2;

public class Singleton {

    private static Singleton uniqueInstance;

    private Singleton() {}

    // TODO Implement me!
    // What's missing in this Singleton declaration?

    public static synchronized Singleton getInstance() {
        if (uniqueInstance == null){
            uniqueInstance = new Singleton();
        }
        // TODO Implement me!
        return uniqueInstance;
    }
}
