package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class GoatCheeseTest {

    private GoatCheese goatCheese;

    @Before
    public void setUp() {
        goatCheese = new GoatCheese();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Goat Cheese", goatCheese.toString());
    }


}
