package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

public class DepokPizzaStoreTest {

    PizzaStore depokPizzaStore;

    @Before
    public void setUp() {
        depokPizzaStore = new DepokPizzaStore();
    }

    @Test
    public void testCheesePizza() {
        Pizza cheesePizza = depokPizzaStore.orderPizza("cheese");
        assertEquals("Depok Style Cheese Pizza", cheesePizza.getName());
    }
    
    @Test
    public void testVeggiePizza() {
        Pizza veggiePizza = depokPizzaStore.orderPizza("veggie");
        assertEquals("Depok Style Veggie Pizza", veggiePizza.getName());
    }

    @Test
    public void testClamPizza() {
        Pizza clamPizza = depokPizzaStore.orderPizza("clam");
        assertEquals("Depok Style Clam Pizza", clamPizza.getName());
    }


}
