package id.ac.ui.cs.advprog.tutorial1.strategy;

public class FlyWithWings implements FlyBehavior{

    public FlyWithWings(){

    }
    @Override
    public void fly() {
        System.out.println("Flapping wings and ready to fly!");
    }
}