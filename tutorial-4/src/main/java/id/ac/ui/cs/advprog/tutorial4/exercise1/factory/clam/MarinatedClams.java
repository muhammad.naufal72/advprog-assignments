package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class MarinatedClams implements Clams {

    public String toString() {
        return "Marinated Clams from Banten";
    }
}
