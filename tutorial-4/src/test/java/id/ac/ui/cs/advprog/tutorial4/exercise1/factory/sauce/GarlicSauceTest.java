package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class GarlicSauceTest {

    private GarlicSauce garlicSauce;

    @Before
    public void setUp() {
        garlicSauce = new GarlicSauce();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Garlic Sauce", garlicSauce.toString());
    }


}
