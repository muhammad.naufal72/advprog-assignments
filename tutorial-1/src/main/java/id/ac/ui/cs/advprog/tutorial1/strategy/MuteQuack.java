package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MuteQuack implements QuackBehavior {
    
    public MuteQuack(){
        
    }

    @Override
    public void quack(){
        System.err.println("...");
    }
}