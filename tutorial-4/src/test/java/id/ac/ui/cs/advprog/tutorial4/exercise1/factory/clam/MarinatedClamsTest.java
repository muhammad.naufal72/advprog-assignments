package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class MarinatedClamsTest {

    private MarinatedClams marinatedClams;

    @Before
    public void setUp() {
        marinatedClams = new MarinatedClams();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Marinated Clams from Banten", marinatedClams.toString());
    }


}
