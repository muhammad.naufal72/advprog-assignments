package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class FilledCrustDoughTest {

    private FilledCrustDough filledCrustDough;

    @Before
    public void setUp() {
        filledCrustDough = new FilledCrustDough();
    }

    @Test
    public void testMethodToString() {
        assertEquals("FilledCrusst style cheese filled crust dough", filledCrustDough.toString());
    }


}
