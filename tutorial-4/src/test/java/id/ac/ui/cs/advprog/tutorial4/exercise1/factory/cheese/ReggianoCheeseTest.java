package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ReggianoCheeseTest {

    private ReggianoCheese regianoCheese;

    @Before
    public void setUp() {
        regianoCheese = new ReggianoCheese();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Reggiano Cheese", regianoCheese.toString());
    }


}
