package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class FilledCrustDough implements Dough {
    public String toString() {
        return "FilledCrusst style cheese filled crust dough";
    }
}
